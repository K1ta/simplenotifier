package notifier.email;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import notifier.AbstractNotifier;

public class EmailNotifier extends AbstractNotifier {

    @Override
    protected void sendNotification(Object json) throws Exception {
        logger.info("start sending notification..");
        ObjectMapper mapper = new ObjectMapper();
        EmailContext context = mapper.convertValue(json, EmailContext.class);
        if (!context.isValid()) {
            throw new IllegalArgumentException("Some fields are not filled");
        }
        logger.info("FROM = " + context.getFrom());
        logger.info("TO = " + context.getTo());
        logger.info("SUBJECT = " + context.getSubject());
        logger.info("HTMLBODY = " + context.getHtmlbody());
        logger.info("TEXTBODY = " + context.getTextbody());

        //create client
        AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder
                .standard().withRegion(Regions.US_WEST_2).build();
        logger.info("Client created");

        //create message body
        Body body = new Body();
        context.getHtmlbody().ifPresent(html -> body.setHtml(new Content().withCharset("UTF-8").withData(html)));
        context.getTextbody().ifPresent(text -> body.setText(new Content().withCharset("UTF-8").withData(text)));
        logger.info("Body created, body=" + body);

        //create message
        Message message = new Message();
        message.setSubject(new Content().withCharset("UTF-8").withData(context.getSubject()));
        message.setBody(body);
        logger.info("Message created, message=" + message);

        SendEmailRequest request = new SendEmailRequest();
        //set source
        request.setSource(context.getFrom());
        logger.info("Set source done");
        //set destination
        request.setDestination(new Destination().withToAddresses(context.getTo()));
        logger.info("Set destination done");
        //set message
        request.setMessage(message);
        logger.info("Set message done");

        SendEmailResult result = client.sendEmail(request);
        logger.info("Email sent! ID = " + result.getMessageId());
    }

}

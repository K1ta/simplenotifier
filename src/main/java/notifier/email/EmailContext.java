package notifier.email;

import java.util.Optional;

public class EmailContext {

    // Replace sender@example.com with your "From" address.
    // This address must be verified with Amazon SES.
    private String from;

    // Replace recipient@example.com with a "To" address. If your account
    // is still in the sandbox, this address must be verified.
    private String to;

    // The subject line for the email.
    private String subject;

    // The HTML body for the email.
    private Optional<String> htmlbody = Optional.empty();

    // The email body for recipients with non-HTML email clients.
    private Optional<String> textbody = Optional.empty();

    public boolean isValid() {
        return from != null && to != null && subject != null && (htmlbody.isPresent() || textbody.isPresent());
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Optional<String> getHtmlbody() {
        return htmlbody;
    }

    public void setHtmlbody(String htmlbody) {
        this.htmlbody = Optional.ofNullable(htmlbody);
    }

    public Optional<String> getTextbody() {
        return textbody;
    }

    public void setTextbody(String textbody) {
        this.textbody = Optional.ofNullable(textbody);
    }
}

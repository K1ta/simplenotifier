package notifier;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractNotifier implements RequestHandler<Object, String> {
    protected static final Logger logger = Logger.getLogger(AbstractNotifier.class.getName());

    private static final String ILLEGAL_ARGUMENT = "Email context is not valid";
    private static final String FAIL = "The notification was not sent";
    private static final String SUCCESS = "OK";

    @Override
    public String handleRequest(Object request, Context context) {
        try {
            sendNotification(request);
        } catch (IllegalArgumentException e) {
            return ILLEGAL_ARGUMENT;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "The notification was not sent", e);
            return FAIL;
        }
        return SUCCESS;
    }

    protected abstract void sendNotification(Object json) throws Exception;

}

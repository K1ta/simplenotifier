# Simple Notifier

Сервис оповещений на AWS Lambda

## EmailNotifier

Функция для отправки email-оповещений

### Входные параметры

Принимает на вход json со следующими параметрами:
* from: email отправителя
* to: email получателя
* subject: тема письма
* htmlbody: html-тело письма
* textbody: текст письма

Входной json может не содержать htmlbody или textbody

### Возвращаемые параметры
Возвращает `ОК` в случае успешной отправкии уведомления или сообщение об ошибке

### Запуск

Так можно собрать проект:

```
$ gradle build
```

В папке build/libs будет создан файл `AwsLambda-email-[version].jar`

Этот файл можно загрузить в AWS Lambda и вызывать с помощью invoke().
Для работы функции необходимо дать ей два разрешения: AmazonSESFullAccess
и AWSLambdaRole.